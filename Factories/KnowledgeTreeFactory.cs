﻿using KnowledgeModule.Models;
using KnowledgeModule.Notifications;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KnowledgeModule.Factories
{
    public class KnowledgeTreeFactory
    {

        private clsLogStates logStates = new clsLogStates();

        public async Task<FactoryResult> CreateKnowledgeTree(List<clsOntologyItem> knowledgeTreeItems,
            List<clsObjectRel> knowledgeTreeRel)
        {
            var knowledgeTree = new List<KnowledgeNetItem>();
            (from knowledgeItem in knowledgeTreeItems
             join knowledgeRel in knowledgeTreeRel on knowledgeItem.GUID equals knowledgeRel.ID_Other into knowledgeRels
             from knowledgeRel in knowledgeRels.DefaultIfEmpty()
             where knowledgeRel == null
             select new KnowledgeNetItem
             {
                 Id = knowledgeItem.GUID,
                 Name = knowledgeItem.Name
             }).OrderBy(knowTree => knowTree.Name).ToList().ForEach(knowledgeItem =>
             {
                 knowledgeTree.Add(knowledgeItem);
                 AddSubItems(knowledgeItem, knowledgeTreeRel);
             });

            var rootItem = knowledgeTree.FirstOrDefault();
            if (rootItem != null)
            {
                rootItem.Selected = true;
            }
            
            var result = new FactoryResult
            {
                Result = logStates.LogState_Success.Clone(),
                KnowledgeTree = knowledgeTree
            };

            return result;
        }

        
        private clsOntologyItem AddSubItems(KnowledgeNetItem parentItem, List<clsObjectRel> knowledgeTreeRel)
        {
            knowledgeTreeRel.Where(relItem => relItem.ID_Object == parentItem.Id).OrderBy(knowTree => knowTree.Name_Other).ToList().ForEach(relItem =>
            {
                var treeItem = new KnowledgeNetItem
                {
                    Id = relItem.ID_Other,
                    Name = relItem.Name_Other
                };

                parentItem.SubItems.Add(treeItem);
                AddSubItems(treeItem, knowledgeTreeRel);
            });

            return logStates.LogState_Success.Clone();
        }

        
    }

    public class FactoryResult
    {
        public clsOntologyItem Result { get; set; }
        private List<KnowledgeNetItem> knowledgeTree;
        public List<KnowledgeNetItem> KnowledgeTree
        {
            get
            {
                return knowledgeTree;
         
            }
            set
            {
                knowledgeTree = value;
                
            }

        }
    }
}

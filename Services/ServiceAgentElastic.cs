﻿using KnowledgeModule.Notifications;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KnowledgeModule.Services
{
    public class ServiceAgentElastic 
    {
        private Globals globals;

        public async Task<KnowledgeTreeResult> GetKnowledgeTree()
        {
            var result = new KnowledgeTreeResult();
            var searchResult1 = GetKnowledgeItems();
            if (searchResult1.Result.GUID == globals.LState_Error.GUID)
            {
                result.Result = searchResult1.Result;
                return result;
            }

            result.Result = searchResult1.Result;
            result.KnowledgeItems = searchResult1.ResultList;

            
            var searchResult2 = GetKnowledgeRels(result.KnowledgeItems);

            if (searchResult2.Result.GUID == globals.LState_Error.GUID)
            {
                result.Result = searchResult2.Result;
                return result;
            }

            result.KnowledgeItemsRel = searchResult2.ResultList;


            return result;
        }


        private SearchResult<clsOntologyItem> GetKnowledgeItems()
        {
            var search = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = "0e4ebffd02364420a9902702eb6618fa"
                }
            };

            var dbReader = new OntologyModDBConnector(globals);

            var result = dbReader.GetDataObjects(search);

            if (result.GUID == globals.LState_Success.GUID)
            {
                return new SearchResult<clsOntologyItem>
                {
                    Result = result,
                    ResultList = dbReader.Objects1
                };
            }
            else
            {
                return new SearchResult<clsOntologyItem>
                {
                    Result = result,
                    ResultList = null
                };
            }

        }

        private SearchResult<clsObjectRel> GetKnowledgeRels(List<clsOntologyItem> knowledgeItems)
        {
            var search = knowledgeItems.Select(knowledge => new clsObjectRel
            {
                ID_Object = knowledge.GUID,
                ID_RelationType = "600ec4c2430d46049c1a797f19be035d",
                ID_Parent_Other = "0e4ebffd02364420a9902702eb6618fa"
            }).ToList();

            if (search.Any())
            {
                var dbReader = new OntologyModDBConnector(globals);

                var result = dbReader.GetDataObjectRel(search);

                if (result.GUID == globals.LState_Success.GUID)
                {
                    return new SearchResult<clsObjectRel>
                    {
                        Result = result,
                        ResultList = dbReader.ObjectRels
                    };
                }
                else
                {
                    return new SearchResult<clsObjectRel>
                    {
                        Result = result,
                        ResultList = null
                    };

                }
            }
            else
            {
                return new SearchResult<clsObjectRel>
                {
                    Result = globals.LState_Success.Clone(),
                    ResultList = new List<clsObjectRel>()
                };

            }

        }

        public ServiceAgentElastic(Globals globals)
        {
            this.globals = globals;
        }
    }

    public class SearchResult<TType>
    {
        public clsOntologyItem Result { get; set; }
        public List<TType> ResultList { get; set; }
    }

    public class KnowledgeTreeResult
    {
        public clsOntologyItem Result { get; set; }

        private List<clsOntologyItem> knowledgeItems;
        public List<clsOntologyItem> KnowledgeItems
        {
            get { return knowledgeItems; }
            set
            {
                knowledgeItems = value;   
            }
        }

        private List<clsObjectRel> knowledgeItemsRel;
        public List<clsObjectRel> KnowledgeItemsRel
        {
            get { return knowledgeItemsRel; }
            set
            {
                knowledgeItemsRel = value;
            }
        }
    }
}

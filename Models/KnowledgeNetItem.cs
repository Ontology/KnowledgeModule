﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeModule.Models
{
    public class KnowledgeNetItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }

        public Dictionary<string, object> JsonDict
        {
            get
            {
                var dict = new Dictionary<string, object>();

                dict.Add("href", "#_" + Id);
                dict.Add("text", Name);
                var stateItem = new Dictionary<string, object>();
                dict.Add("state", stateItem);
                stateItem.Add("selected", Selected);
                if (SubItems != null && SubItems.Any())
                {
                    dict.Add("nodes", SubItems.Select(subItem => subItem.JsonDict));
                }

                return dict;
            }
        }

        
        public List<KnowledgeNetItem> SubItems { get; set; }

        public KnowledgeNetItem()
        {
            SubItems = new List<KnowledgeNetItem>();
        }
    }
}